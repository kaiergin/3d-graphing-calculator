public class Line {
	public double[] directionV = new double[3];
	public double[] positionV = new double[3];
	public Line(double[] direction, double[] point) {
		System.arraycopy(direction, 0, this.directionV, 0, direction.length);
		System.arraycopy(point, 0, this.positionV, 0, point.length);
	}
}