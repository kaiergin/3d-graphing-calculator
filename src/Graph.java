/*
 * Written by Kai Ergin
 */
import java.lang.Math;

public class Graph {
	
	// Used to count if pixels fail to graph (for debugging)
	public static int graphCounter = 0;
	
	// Method to graph any equation of your choosing
	// Does not work at the moment
	public double placeFunc(Line check) {
		double t = 0;
		double amount = 2;
		boolean finished = false;
		double y = 1;
		double x = 1;
		double z = 1;
		for (int w = 0; w < 20; w++) {
			x = t*check.directionV[0] + check.positionV[0];
			y = t*check.directionV[1] + check.positionV[1];
			z = t*check.directionV[2] + check.positionV[2];
			double eval = x*x + y*y; // equation to graph
			if (Math.abs(eval - z) < .001) {
				finished = true;
				break;
			}
			if (eval > z) {
				t += amount;
			} else {
				t -= amount;
			}
			amount /= 2;
		}
		if (finished) {
			if (Math.abs(x) >= .5 || Math.abs(y) >= .5 || Math.abs(z) >= .5) {
				return -1;
			} else {
				return t;
			}
		} else {
			graphCounter++;
		}
		return -1;
	}
	
	// Method to graph a paraboloid with gradient mesh
	public double[] placeParaMesh(Line check) {
		double[] badIntersection = {-1,-1,-1};
		// Calculate paraboloid
		double i = Math.pow(check.directionV[0], 2) + Math.pow(check.directionV[1], 2);
		double j = 2*check.directionV[0]*check.positionV[0]
				+ 2*check.directionV[1]*check.positionV[1] - check.directionV[2];
		double k = Math.pow(check.positionV[0], 2)
				+ Math.pow(check.positionV[1], 2) - check.positionV[2];
		
		// If line does not intersect
		if (j*j - 4*i*k < 0) {
			return badIntersection;
		}
		
		// Find distance for intersection
		double a = (-1*j + Math.sqrt(j*j - 4*i*k))/(2*i);
		double b = (-1*j - Math.sqrt(j*j - 4*i*k))/(2*i);
		
		// Creates possible arrays that will be returned
		double aa = a*check.directionV[0] + check.positionV[0];
		double bb = a*check.directionV[1] + check.positionV[1];
		double cc = a*check.directionV[2] + check.positionV[2];
		double dd = b*check.directionV[0] + check.positionV[0];
		double ee = b*check.directionV[1] + check.positionV[1];
		double ff = b*check.directionV[2] + check.positionV[2];
		// One array for each intersection
		double[] myA = {aa,bb,cc,a};
		double[] myB = {dd,ee,ff,b};
		
		// Cuts off if distance > graphingArea
		double graphingArea = .5;
		
		// For checking if first and second intersection are in graphing area
		boolean aTrue = true;
		boolean bTrue = true;
		if (Math.abs(aa) >= graphingArea
				|| Math.abs(bb) >= graphingArea
				|| Math.abs(cc) >= graphingArea) {
			aTrue = false;
		}
		if (Math.abs(dd) >= graphingArea || Math.abs(ee) >= graphingArea || Math.abs(ff) >= graphingArea) {
			bTrue = false;
		}
		
		// Places grid onto graph
		double thickness = .005;
		if (Math.abs(aa%.1) < thickness || Math.abs(bb%.1) < thickness) {
			myA[0] = 0;
			myA[1] = 0;
			myA[2] = 0;
		}
		if (Math.abs(dd%.1) < thickness || Math.abs(ee%.1) < thickness) {
			myB[0] = 0;
			myB[1] = 0;
			myB[2] = 0;
		}
		
		// Finds which intersection is closer to camera
		if (a < b && aTrue) {
			return myA;
		} else if (bTrue) {
			return myB;
		} else if (aTrue) {
			return myA;
		}
		return badIntersection;
	}
	
	// Checks if a line intersects with a coordinate axis
	// Axis is represented as a cylinder
	public double placeAxis(Line check, int r, int s) {
		// Radius of cylinder
		double radius = .00001;
		// Calculate cylinder intersection
		double a = Math.pow(check.directionV[r], 2) + Math.pow(check.directionV[s], 2);
		double b = 2*check.directionV[r]*check.positionV[r] + 2*check.directionV[s]*check.positionV[s];
		double c = Math.pow(check.positionV[r], 2) + Math.pow(check.positionV[s], 2) - radius;
		// Line does not intersect with axis
		if (b*b - 4*a*c < 0) {
			return -2;
		}
		// Find intersection
		double e = (-1*b + Math.sqrt(b*b - 4*a*c))/(2*a);
		// Cuts off if distance > graphingArea
		double graphingArea = .5;
		if (Math.abs(e*check.directionV[0] + check.positionV[0]) <= graphingArea
				&& Math.abs(e*check.directionV[1] + check.positionV[1]) <= graphingArea
				&& Math.abs(e*check.directionV[2] + check.positionV[2]) <= graphingArea) {
			return e;
		}
		return -2;
	}

	// Simulates a lens by making image originate from a point onto a screens
	public Line[][] genScreen (double phi, double theta){
	  	// Uses spherical coordinates
	  	// Finds point
	  	double x = Math.sin( phi ) * Math.cos( theta );
	  	double y = Math.sin( phi ) * Math.sin( theta );
	  	double z = Math.cos( phi );
	    
	  	// Second point
	 	double s = 2;
		double xx = s*x;
		double yy = s*y;
		double zz = s*z;

		// Screen
		double horzVect[] = { -1 * Math.sin( theta ), Math.cos( theta ), 0 };
		double vertVect[] = { Math.cos( theta ) * Math.cos( phi ),
				Math.sin( theta ) * Math.cos( phi ),
				-1 * Math.sin( phi ) }; 
		
		int size = 500;
		Line[][] vectors = new Line[size][size];
		for (int a = 0; a < size; a++) {
			for (int b = 0; b < size; b++) {
				// Points on screen
				double i = (double)(a - (size/2)) * horzVect[0] /size + x
						+ (double)(b - (size/2)) * vertVect[0] /size;
				double j = (double)(a - (size/2)) * horzVect[1] /size + y
						+ (double)(b - (size/2)) * vertVect[1] /size;
				double k = (double)(b - (size/2)) * vertVect[2] /size + z;
				
				double[] arg = {i, j, k};
				double[] dirVector = { i - xx, j - yy, k - zz };
				vectors[a][b] = new Line(dirVector, arg);
			}
		}
		return vectors;
	}
}
