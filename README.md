# 3d-graphing-calc
This is a 3D graphing calculator made from scratch.

Purposes:
Create a 3D graphing calculator that can graph 3D functions and surfaces in a GUI.
Simulate a 3D environment.
Allow the user to change the camera angle to view the graph from multiple perspectives.

Run using the compile file or the jar file

![Image of Paraboloid]
(https://bitbucket.org/kaiergin/3d-graphing-calculator/raw/84f18e5ec0078e05c959f3914988c0193d9430ec/Screenshot1.png)

![Image of Paraboloid 2]
(https://bitbucket.org/kaiergin/3d-graphing-calculator/raw/84f18e5ec0078e05c959f3914988c0193d9430ec/Screenshot2.png)

Written by:
Kai Ergin & Allston Mickey
